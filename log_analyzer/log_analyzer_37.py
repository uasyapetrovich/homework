#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';

import re
import os
import gzip
import logging
import argparse
import json
import sys

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./",
    "LOG_DIR": "./",
    "LOGGING": None
}


def main(config_local):
    config_local = make_config(config_local)
    chosen_file = choose_file(config_local['LOG_DIR'])
    parsed_file = parsing_gen(chosen_file)
    if chosen_file != 'no log-files':
        try:
            count(parsed_file, chosen_file)
        except Exception:
            logging.exception('Unexpected error')
            raise
    else:
        logging.info('exiting'), sys.exit()


def make_config(default_conf):
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--config', dest='conf_path', default=default_conf)
    args = arg_parser.parse_args()

    try:
        if args.conf_path != default_conf:
            with open(args.conf_path, "r") as config_file:
                data = json.load(config_file)
            # config.update(data)   # bad way
            for key in default_conf.keys():  # good way
                if key in data.keys():
                    default_conf[key] = data[key]
    except Exception:
        print('Invalid config file')
        raise
    check_config(default_conf)
    logging.info('config updated from: ' + str(args.conf_path))
    return default_conf


def check_config(conf):
    try:
        logging.basicConfig(level=logging.DEBUG,
                            filename=conf['LOGGING'],
                            format='%(asctime)s %(levelname)s %(message)s',
                            filemode="w")
    except Exception:
        print('Path for logging is not available:', conf['LOGGING'])
        raise

    if not os.path.exists(conf['LOG_DIR']) or not os.access(conf['LOG_DIR'], os.R_OK):
        logging.error('Not available directory to analyze (LOG_DIR): ' + conf['LOG_DIR'])
        raise NotADirectoryError('Not available directory to analyze (LOG_DIR)')

    if not os.path.exists(conf['REPORT_DIR']) or not os.access(conf['REPORT_DIR'], os.W_OK):
        logging.error('Not available directory to save report (REPORT_DIR): ' + conf['REPORT_DIR'])
        raise NotADirectoryError('Not available directory to save report (REPORT_DIR)')

    if not isinstance(conf['REPORT_SIZE'], int):
        logging.error('REPORT_SIZE must be an integer')
        raise TypeError('REPORT_SIZE must be an integer')


def choose_file(log_dir):
    log_file = 'no log-files'
    log_name = 'nginx-access-ui.log-'
    date = 0
    for file in os.listdir(log_dir):
        file_date = file[len(log_name):len(log_name) + 8]

        if not file_date.isdigit():
            logging.info('ignored file: %s' % file)
            continue

        if file.startswith(log_name) and date < int(file_date):
            if file.endswith(file_date) or file.endswith(file_date + '.gz'):
                log_file = os.path.join(log_dir, file)
            else:
                print('non-format log-file: %s' % file)
                continue

            date = int(file_date)
            logging.info('listing log-files: %s' % file)
    else:
        logging.info('opening log-file: %s' % log_file)
        return log_file


def parsing_gen(log_file):
    opener = gzip.open if log_file.endswith('.gz') else open
    try:
        with opener(log_file, 'rt') as file:
            for line in file:
                url = re.findall(r' (.+) ', str(re.findall(r'GET .+ HTTP|POST .+ HTTP|PUT.+ '
                                                           r'HTTP|HEAD .+ HTTP|OPTIONS.+ HTTP', line)))
                request_time = re.findall(r'\d+\.\d+$', line)
                try:
                    yield url[0], request_time[0]
                except IndexError:
                    logging.exception('No URL in line: ' + line)
                    continue
                except Exception:
                    logging.exception('Unexpected error while parsing line: ' + line)
                    continue
    except KeyboardInterrupt as e:
        logging.exception('while parsing file: %s\n%s' % (log_file, e))
        raise
    except Exception as e:
        logging.exception('while parsing file: %s\n%s' % (log_file, e))
        raise


def count(parsed_file, log_file):
    stats = {}
    try:
        for line in parsed_file:
            url = line[0]
            request_time = line[1]
            if url in stats:
                stats[url].append(request_time)
            else:
                stats[url] = [request_time]

    except KeyboardInterrupt as e:
        logging.exception('while parsing file: %s\n%s' % (log_file, e))
        logging.info(stats)
        raise
    except Exception as e:
        logging.exception('while parsing file: %s\n%s' % (log_file, e))
        raise

    """
    count = 0   # сколько раз встречается URL, абсолютное значение
    count_perc = 0   # сколько раз встречается URL, в процентнах относительно общего числа запросов
    time_sum = 0   # суммарный \$request_time для данного URL'а, абсолютное значение
    time_perc = 0   # суммарный \$request_time для данного URL'а, в процентах относительно общего $request_time всех запросов
    time_avg = 0   # средний \$request_time для данного URL'а
    time_max = 0   # максимальный \$request_time для данного URL'а
    time_med = 0   # медиана \$request_time для данного URL'а
    """


if __name__ == "__main__":
    main(config)
